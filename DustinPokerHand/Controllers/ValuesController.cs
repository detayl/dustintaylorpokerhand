﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.ComponentModel;
using System.Reflection;

namespace DustinPokerHand.Controllers
{
        public enum Face { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack , Queen, King, Ace }
        public enum Suit { Clubs, Diamonds, Hearts, Spades }
        public enum HandResult { Blank, [Description("High Card")]HighCard, Pair, [Description("Two Pair")]TwoPair, [Description("Three of a Kind")]ThreeOfAKind,
            Straight, Flush, [Description("Full House")]FullHouse, [Description("Four of a Kind")]FourOfAKind, [Description("Straight Flush")]StraightFlush }
    

   
    public class Card
    {
        public Face face { get; set; }
        public Suit suit { get; set; }
    }

    public class EvaluatedCards
    {
        public List<Cards> evalCards { get; set; }
    }

    public class Cards
    {
        public List<Card> cards { get; set; }
    }

    public class Hand
    {
        public string name { get; set; }
        public string[] hand { get; set; }
    }
    public class Hands 
    {
        public List<Hand> hands { get; set; } 
    }
    public class ParsedHands
    {
        public List<Cards> someParsedHands { get; set; }
    }

    public class EvaluateHand
    {

        public bool CheckStraight(ParsedHands checkCards, int num)
        {
            var cards = checkCards.someParsedHands[num].cards.GroupBy(g => g.face).OrderByDescending(s => s.Key);
            int sequence = 0;
            int d = 0;
            int v = 0;
            foreach(var c in cards)
            {
                foreach(var e in c)
                {
                    d = Convert.ToInt32(e.face);
                }


                if (v == 0)
                {
                    v = d;
                    sequence++;
                }
                else
                {
                    if (v - d == 1)
                    {
                        sequence++;
                        v = d;
                    }
                    else
                        break;
                }
            }
            if (sequence == 5)
                    return true;
                else
                    return false;
        }

        public bool CheckFlush (ParsedHands checkCards, int num)
        {
            var cards = checkCards.someParsedHands[num].cards.GroupBy(g => g.suit).OrderByDescending(s => s.Count());
            foreach (var d in cards)
            {
                if (d.Count() > 4)
                {
                    return true;
                }
            }
            return false;
        }

        public List<HandResult> CheckHands(ParsedHands parsed)
        {
            List<HandResult> result = new List<HandResult>();
            bool possibleFH = false;
            bool possibleTwoPair = false;
            HandResult HR = new HandResult();

            for (int i = 0; i < 2; i++)
            {
                var xCards = parsed.someParsedHands[i].cards.GroupBy(g => g.face).OrderByDescending(s => s.Count());

                foreach (var grp in xCards)
                {
                    int count = grp.Count();
                    if (count > 1)
                    {
                        if (count == 4)
                        {
                            HR = HandResult.FourOfAKind;
                            break;
                        }
                        else if (count == 3)
                        {
                            possibleFH = true;
                            HR = HandResult.ThreeOfAKind;
                        }
                        else if (count == 2 && possibleFH == true)
                        {
                            HR = HandResult.FullHouse;
                            break;
                        }
                        else if (count == 2 && possibleTwoPair == true)
                        {
                            HR = HandResult.TwoPair;
                            break;
                        }
                        else if (count == 2)
                        {
                            possibleTwoPair = true;
                            HR = HandResult.Pair;
                        }
                    }   
                    else
                    {
                        possibleFH = false;
                        possibleTwoPair = false;

                        if (HR == HandResult.Blank)
                        {
                            HR = HandResult.HighCard;
                            bool isFlush = CheckFlush(parsed, i);
                            if (isFlush)
                            {
                                HR = HandResult.Flush;
                                bool isStraight = CheckStraight(parsed, i);
                                if (isStraight)
                                    HR = HandResult.StraightFlush;
                                break;
                            }
                            else
                            {
                                bool isStraight = CheckStraight(parsed, i);
                                if (isStraight)
                                    HR = HandResult.Straight;
                                break;
                            }
                        }
                    }
                }
                result.Add(HR);
                HR = HandResult.Blank;
            }
            return result;
         }
        
        public int CompareHands(List<HandResult> res)
        {
            if(res[0].CompareTo(res[1]) == -1)
            {
                return 1;
            }
            else if (res[0].CompareTo(res[1]) == 0)
            {
                return 3;
            }
            else
            return 0;
        }
    }
    public class EnumDescriptions
        {

            public string ShowEnumDescription(HandResult hand)
            {
                FieldInfo fi = hand.GetType().GetField(hand.ToString());
                DescriptionAttribute[] des = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (des != null && des.Length > 0)
                    return des[0].Description;
                else
                    return hand.ToString();
            }
        }

    public class ParseHand
    {
        //Changes the string inputs from the HTML pages to strings that directly correlate to the Enums
        public string ConvertStringToFace (string str)
        {
            str = str.ToUpper();

            switch (str)
            {
                case "A":
                    str = "Ace";
                    break;
                case "K":
                    str = "King";
                    break;
                case "Q":
                    str = "Queen";
                    break;
                case "J":
                    str = "Jack";
                    break;
                default:
                    break;
            }
            return str;
        }

        //Changes the string inputs from the HTML pages to strings that directly correlate to the Enums
        public string ConvertStringToSuit(string str)
        {
            str = str.ToUpper();

            switch (str)
            {
                case "D":
                    str = "Diamonds";
                    break;
                case "H":
                    str = "Hearts";
                    break;
                case "C":
                    str = "Clubs";
                    break; 
                case "S":
                    str = "Spades";
                    break;
                default:
                    break;
            }
            return str;
        }

        //Changing the string values to Enums and returning a list of both hands in Enums
        public ParsedHands DoParse(Hands hands)
        {
            ParsedHands parsed = new ParsedHands();
            parsed.someParsedHands = new List<Cards>();
            List<Card> parsedCards = new List<Card>();
            List<Card> parsedCards1 = new List<Card>();
            
            //First for loop set for two players to loop and parse each of the hands to enums
            for (int i = 0; i < 2; i++)
            {
                var h = hands.hands[i].hand;
                //Second for loop set to parse each card in the hand to enums for easy evaluation
                for (int j = 0; j < h.Length; j++)
                {
                    
                    if (h[j].Length == 3)
                    {
                        try
                        {
                            if (i == 0)
                            {
                                var f = (Enum.Parse((typeof(Face)), ConvertStringToFace(h[j].Substring(0, 2))));
                                var s = (Enum.Parse((typeof(Suit)), ConvertStringToSuit(h[j].Substring(2, 1))));
                                parsedCards.Add(new Card() { face = (Face)f, suit = (Suit)s });
                            }
                            else
                            {
                                var f = (Enum.Parse((typeof(Face)), ConvertStringToFace(h[j].Substring(0, 2))));
                                var s = (Enum.Parse((typeof(Suit)), ConvertStringToSuit(h[j].Substring(2, 1))));
                                parsedCards1.Add(new Card() { face = (Face)f, suit = (Suit)s });
                            }
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (i == 0)
                            {
                                var f = (Enum.Parse((typeof(Face)), ConvertStringToFace(h[j].Substring(0, 1))));
                                var s = (Enum.Parse((typeof(Suit)), ConvertStringToSuit(h[j].Substring(1, 1))));
                                parsedCards.Add(new Card() { face = (Face)f, suit = (Suit)s });
                            }
                            else
                            {
                                var f = (Enum.Parse((typeof(Face)), ConvertStringToFace(h[j].Substring(0, 1))));
                                var s = (Enum.Parse((typeof(Suit)), ConvertStringToSuit(h[j].Substring(1, 1))));
                                parsedCards1.Add(new Card() { face = (Face)f, suit = (Suit)s });
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }

                if (i == 0)
                    parsed.someParsedHands.Add(new Cards() { cards = parsedCards });
                else
                    parsed.someParsedHands.Add(new Cards() { cards = parsedCards1 });
            }
            //return the two hands to the calling function
            return parsed;
        }
    }
    public class HandsController : ApiController
    {
        
        // GET api/hands/
        public int Get()
        {
            return 1;
        }

        // GET api/hands/5/
        public string Get(int id)
        {
            return "value";
        }

        // POST api/hands/
        public HttpResponseMessage Post(Hands hands)
        {
            ParseHand parse = new ParseHand();
            ParsedHands parsed = parse.DoParse(hands);
            EvaluateHand evaluate = new EvaluateHand();
            List<HandResult> HR = evaluate.CheckHands(parsed);
            int v = evaluate.CompareHands(HR);
            EnumDescriptions ed = new EnumDescriptions();
            
            //Need Item here to call evaluator Function using the parsed hands as input.
            //Need Second item to evaluate the values of the two and return the result of both hands evaluation.
            //Finally selecting either 0 or 1 as to who wins with a variable and inserting variable where v lies in code below.
            //If result is a tie, v is supplied with a '3' and response to website notates it's a tie.

            if (v == 3)
                return this.Request.CreateResponse(HttpStatusCode.OK, new { result = "There is a tie." });
            else
            return this.Request.CreateResponse(HttpStatusCode.OK, new { result = hands.hands[v].name + " wins the hand with a " + ed.ShowEnumDescription(HR[v]) + "." });
        }

        // PUT api/hands/
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/hands/5/
        public void Delete(int id)
        {
        }
    }
}
